[![pipeline status](https://gitlab.com/quintgroup/cicd-hands-on-nl/badges/master/pipeline.svg)](https://gitlab.com/quintgroup/cicd-hands-on-nl/commits/master)
# CI/CD & Automation Hands-on Getting Started 

Dit project kan je inzetten bij een CI/CD of DevOps training. Hoe ziet het voortbrengingsproces er vanuit ontwikkelaars perspectief er uit? Hiervoor start je vanuit Gitlab en ga je daarna aan de slag met automatisering in de context van CI/CD. 

- [Les 1 Hands-on introductie Gitlab](Les 1)
- [Les 2 Provisioning.. de bodem](Les 2)
- [Les 3 Containers en Orchestration](Les 3)

Voor deze training wordt vanuit Quint voorzien in de juiste toegang tot de tooling, deze wordt vanuit public cloud diensten van GitLab.com, Google GCP afgenomen. 

## Voor instructeurs of mensen die zelf aan de slag willen met dit repository
Mocht je deze oefeningen in eigen tempo zelf willen uitvoeren dan is het mogelijk een trial aan te maken bij Google (geen kosten, wel credit card nodig of integratie van je project in de quintgroup.com Google organisatie, vraag hiernaar bij je practice lead) en voor Gitlab kan je de gratis versie op GitLab.com inzetten. Er zal binnenkort ook een Webinar van deze training beschikbaar komen, hierbij zit dan ook de instructie voor het initieel opzetten van de tooling.
Door dit project te Forken (speciaal soort kopieren doormiddel van een afsplitsing), kan je de opdrachten (laten) uitvoeren zonder het basismateriaal onbruikbaar te maken.